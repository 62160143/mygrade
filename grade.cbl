       IDENTIFICATION DIVISION.
       PROGRAM-ID.  GRADE.
       AUTHOR. Supakit.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SUB-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT GRADE-FILE ASSIGN TO "AVG.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.  
       FILE SECTION. 
       FD  SUB-FILE.
       01  SUB-DETAILS.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE.
           05 SUB-ID   PIC 9(6).
           05 SUB-NAME PIC X(50).
           05 SUB-UNIT PIC 9(1).
           05 SUB-GRADE PIC X(2).
       FD  GRADE-FILE.
       01  GRADE-DETAILS. 
           05 AVG-GRADE PIC 9(1)V9(3).
           05 AVG-SCI-GRADE PIC 9(1)V9(3).
           05 AVG-CS-GRADE PIC 9(1)V9(3).
       WORKING-STORAGE SECTION. 
       01  SUM-UNIT PIC 9(3)V99.
       01  SUM-SCORE PIC 9(3)V99.
       01  SUM-SCORE-SCI PIC 9(3)V99.
       01  SUM-UNIT-SCI PIC 9(3)V99.
       01  SUB-GRADE-NUM PIC 9(1)V9(2).
       01  SUM-SCORE-CS PIC 9(3)V99.
       01  SUM-UNIT-CS PIC 9(3)V99.
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT SUB-FILE
           OPEN OUTPUT GRADE-FILE 
           MOVE 0 TO SUM-UNIT 
           MOVE 0 TO SUM-SCORE
           MOVE 0 TO SUM-SCORE-SCI 
           MOVE 0 TO SUM-UNIT-SCI 
           PERFORM  UNTIL END-OF-SCORE-FILE 
              READ SUB-FILE
                 AT END SET END-OF-SCORE-FILE TO TRUE
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF 
           END-PERFORM 
           COMPUTE AVG-GRADE  = SUM-SCORE / SUM-UNIT
           DISPLAY "AVG-GRADE : "AVG-GRADE
           COMPUTE AVG-SCI-GRADE = SUM-SCORE-SCI/SUM-UNIT-SCI
           DISPLAY "AVG-SCI-GRADE : "AVG-SCI-GRADE
           COMPUTE AVG-CS-GRADE = SUM-SCORE-CS/SUM-UNIT-CS
           DISPLAY "AVG-CS-GRADE : "AVG-CS-GRADE
           WRITE GRADE-DETAILS
           CLOSE SUB-FILE 
           CLOSE GRADE-FILE
           GOBACK 
           .
       001-PROCESS.
           COMPUTE SUM-UNIT = SUM-UNIT + SUB-UNIT
           EVALUATE TRUE
              WHEN SUB-GRADE = "A"  MOVE 4.00 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "B+"  MOVE 3.50 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "B"  MOVE 3.00 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "C+"  MOVE 2.50 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "C"  MOVE 2.00 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "D+"  MOVE 1.50 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "D"  MOVE 1.00 TO SUB-GRADE-NUM 
              WHEN SUB-GRADE = "F"  MOVE 0 TO SUB-GRADE-NUM 
              
           END-EVALUATE
           COMPUTE SUM-SCORE  = SUM-SCORE + (SUB-UNIT * SUB-GRADE-NUM)         
           IF SUB-ID(1:1) = 3 THEN
              COMPUTE SUM-SCORE-SCI = SUM-SCORE-SCI+
                                      (SUB-UNIT*SUB-GRADE-NUM)
              COMPUTE SUM-UNIT-SCI  = SUM-UNIT-SCI + SUB-UNIT
           .
           IF SUB-ID(1:2) = 31 THEN 
              COMPUTE SUM-SCORE-CS = SUM-SCORE-CS+
                                      (SUB-UNIT*SUB-GRADE-NUM)
              COMPUTE SUM-UNIT-CS  = SUM-UNIT-CS + SUB-UNIT
           .
  
       001-EXIT.
           EXIT.

